Owncloud server on Raspberry PI
=========

This Ansible role mainly installs Owncloud server into a Raspberry PI.

At today, about this role:

- automatic configuration of trusted domains is not supported, it must be done manually;
- MariaDB is the only supported database;
- upgrading an existing Owncloud installation is still not supported;
- Raspberry PI Model 3 is the only tested model.

If you use https://www.dynu.com/en-US as dynamic DNS service, this role:

- create the script to update your public IP on www.dynu.com;
- create the CRON entry for a recurrent updating;

Future steps:

- handling the configuration of trusted domains
- making possible an upgrade of an already existing installation
- configure  _letsencrypt_ on apache 2
- attaching a partition on data directory of Owncloud

Additional notes:

* the task _Step to complete the installation_ took some minutes on my Raspberry PI;



You may read important information on Owncloud at: https://doc.owncloud.org/server/10.4/

Requirements
------------

No requirements.

Role Variables
--------------

You should not need to change variables into vars/main.yml.

You may change variables into defaults/main.yml to personalize your installation; check out below table.

| Variable                 | Values              | Note                                                         |
| ------------------------ | ------------------- | ------------------------------------------------------------ |
| owncloud_package         | owncloud-10.4.0.zip | The file must exists on Owncloud web site.                   |
| database                 | mariadb             |                                                              |
| owncloud_trusted_domains | List                | Lif of trusted domains                                       |
| dynu                     | true\|false         | true if you want to configure the script for updating https://www.dynu.com/en-US |
| dynu_user                |                     | your username on https://www.dynu.com/en-US                  |
| dynu_password            |                     | your password on https://www.dynu.com/en-US                  |


Dependencies
------------

No dependencies on other roles.

Example Playbook
----------------

Here an example of playbook to use this role:

```yaml
---
- hosts: Raspberry
roles:
    - raspberrypi_ownloud
```

You may also override some variables into the playbook:

```yaml
---
- hosts: Raspberry
vars:
    dynu_user: "<username>"
    dynu_password: "<password>"
    owncloud_db_user: "<username>"
    owncloud_db_pwd: "<password>"
    owncloud_admin_user: "<username>"
    owncloud_admin_pwd: "<password>"
roles:
    - raspberrypi_ownloud
```

License
-------

MIT

Author Information
------------------

LosAngeles971.

Web site: https://web.bluepulsar971.it/

Any suggestion to improve this role would be really appreciated (please open an issue in case).
